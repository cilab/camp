package files;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Ex1_Files {

    public static void main(String[] args) throws IOException {
        // pass the path to the file as a parameter
        File file = new File("./resources/text.txt");
        File fileOut = new File("./resources/copy.txt");
        Scanner sc = new Scanner(file);
        FileWriter writer = new FileWriter(fileOut);

        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            //System.out.println(line);
            writer.append(line).append(System.lineSeparator());
        }
        writer.close();
    }
}