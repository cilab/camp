package files;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Ex2_Files {

    public static void main(String[] args) throws IOException {
        // pass the path to the file as a parameter
        File file =  new File("./resources/text.csv");
        File fileOut =  new File("./resources/copy.csv");
        Scanner sc = new Scanner(file);
        sc.useDelimiter("[,"+System.lineSeparator()+"]");
        FileWriter writer = new FileWriter(fileOut);
        int index = 0;
        while (sc.hasNext()) {
            String cell = sc.next();
            cell = cell.trim();
            //System.out.println(cell);
            writer.append(cell);
            if (++index % 4 == 0) {
                writer.append(System.lineSeparator());
            } else {
                writer.append(",");
            }
        }
        writer.close();
    }
}