package webCopyRefactored;

/* *
   Seems like someone did it for us
   https://stackoverflow.com/questions/3422673/how-to-evaluate-a-math-expression-given-in-string-form
 */
public class Ex3_Calculator {
    String expression;
    int position = -1;
    int character = 0;

    void nextChar() {
        character = (++position < expression.length()) ? expression.charAt(position) : -1; // next char or end!
    }

    boolean eat(int charToEat) {
        while (character == ' ') nextChar(); // remove white space
        if (character == charToEat) {
            nextChar();
            return true;
        }
        return false;
    }

    double parse() {
        nextChar();  // read what is next
        double x = parseExpression();
        if (position < expression.length()) throw new RuntimeException("Unexpected: " + (char) character);
        return x;
    }
    // This below is important it shows ho it works!
    // Grammar:
    // expression = term | expression `+` term | expression `-` term
    // term = factor | term `*` factor | term `/` factor
    // factor = `+` factor | `-` factor | `(` expression `)`
    //        | number | functionName factor | factor `^` factor

    double parseExpression() {
        double leftTerm = parseTerm();
        for (; ; ) {
            if (eat('+')) leftTerm += parseTerm(); // addition
            else if (eat('-')) leftTerm -= parseTerm(); // subtraction
            else return leftTerm;
        }
    }

    double parseTerm() {
        double letTerm = parseFactor();
        for (; ; ) {
            if (eat('*')) letTerm *= parseFactor(); // multiplication
            else if (eat('/')) letTerm /= parseFactor(); // division
            else return letTerm;
        }
    }

    double parseFactor() {
        if (eat('+')) return parseFactor(); // unary plus
        if (eat('-')) return -parseFactor(); // unary minus

        double doubleValue;
        int startPosition = this.position;
        if (eat('(')) { // parentheses
            doubleValue = parseExpression();
            eat(')');
        } else if ((character >= '0' && character <= '9') || character == '.') { // numbers
            while ((character >= '0' && character <= '9') || character == '.') nextChar();
            doubleValue = Double.parseDouble(this.expression.substring(startPosition, this.position));
        } else if (character >= 'a' && character <= 'z') { // functions
            while (character >= 'a' && character <= 'z') nextChar();
            String mathFunction = this.expression.substring(startPosition, this.position);
            doubleValue = parseFactor();
            if (mathFunction.equals("sqrt")) doubleValue = Math.sqrt(doubleValue);
            else if (mathFunction.equals("sin")) doubleValue = Math.sin(Math.toRadians(doubleValue));
            else if (mathFunction.equals("cos")) doubleValue = Math.cos(Math.toRadians(doubleValue));
            else if (mathFunction.equals("tan")) doubleValue = Math.tan(Math.toRadians(doubleValue));
            else throw new RuntimeException("Unknown function: " + mathFunction);
        } else {
            throw new RuntimeException("Unexpected: " + (char) character);
        }

        if (eat('^')) doubleValue = Math.pow(doubleValue, parseFactor()); // exponentiation

        return doubleValue;
    }

    public Double evaluate(String expression) {
        this.expression = expression;
        return parse();
    }

}