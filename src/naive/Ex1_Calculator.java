package naive;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Ex1_Calculator {
    public Double evaluate(String expression) {
        Double sum = 0.0;
        for (String summand : expression.split("\\+"))
            sum += Double.valueOf(summand);
        return sum;
    }
}