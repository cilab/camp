import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;
import webCopyRefactored.Ex3_Calculator;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class Ex5_CalculatorTest_webCopyRefactoredAdvanced {


    @ParameterizedTest
    @ValueSource(strings = {"Hello", "World"})
    void shouldPassNonNullMessageAsMethodParameter(String m) {
        assertNotNull(m);
    }

    @ParameterizedTest
    @ValueSource(strings = {"1+2+3 = 6.0", "1+2-3 = 0.0"})
    public void universal(String testExpression) {
        Ex3_Calculator calculator = new Ex3_Calculator();
        String[] expression = testExpression.split("=");

        Double sum = calculator.evaluate(expression[0]);
        assertEquals(Double.parseDouble(expression[1]), sum);
    }

    @DisplayName("Should calculate the correct sum")
    @ParameterizedTest(name = "{index} => expression={0}, expected={1}")
    @CsvSource({
            "1+1, 2",
            "2-1, 1",
            "3/2, 1.5",
            "4*4, 16",
    })
    public void universalCSV(String expression, String expected) {
        Ex3_Calculator calculator = new Ex3_Calculator();

        Double sum = calculator.evaluate(expression);
        assertEquals(Double.parseDouble(expected), sum);
    }


    @DisplayName("Should calculate the correct sum")
    @ParameterizedTest(name = "{index} => expression={0}, expected={1}")
    @CsvFileSource(resources = "test-data.csv")
    public void universalCSVFile(String expression, String expected) {
        Ex3_Calculator calculator = new Ex3_Calculator();

        Double sum = calculator.evaluate(expression);
        assertEquals(Double.parseDouble(expected), sum);
    }

    @DisplayName("Should calculate the correct sum")
    @ParameterizedTest(name = "{index} => expression={0}, expected={1}")
    @MethodSource("sumProvider")
    public void universalProvider(String expression, Double expected) {
        Ex3_Calculator calculator = new Ex3_Calculator();

        Double sum = calculator.evaluate(expression);
        assertEquals(expected, sum);
    }

    private static Stream<Arguments> sumProvider() {
        return Stream.of(
                Arguments.of("2+2+3", 7.0),
                Arguments.of("2-3+1", 0.0)
        );
    }

}
