import org.junit.jupiter.api.Test;
import webCopy.Ex2_Calculator;
import webCopyRefactored.Ex3_Calculator;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Ex4_CalculatorTest_webCopyRefactored {
    @Test
    public void sum() {
        Ex3_Calculator calculator = new Ex3_Calculator();
        Double sum = calculator.evaluate("1+2+3");
        assertEquals(6, sum);
    }

    @Test
    public void addSubtract() {
        Ex3_Calculator calculator = new Ex3_Calculator();
        Double sum = calculator.evaluate("1+2-3");
        assertEquals(0, sum);
    }

    @Test
    public void mult() {
        Ex3_Calculator calculator = new Ex3_Calculator();
        Double sum = calculator.evaluate("1+2*3");
        assertEquals(7, sum);
    }

    @Test
    public void div() {
        Ex3_Calculator calculator = new Ex3_Calculator();
        Double sum = calculator.evaluate("2+6/3");
        assertEquals(4, sum);
    }

    @Test
    public void power() {
        Ex3_Calculator calculator = new Ex3_Calculator();
        Double sum = calculator.evaluate("2+6^2");
        assertEquals(38, sum);
    }
}
