import naive.Ex1_Calculator;

public class Ex1_CalculatorTest_naive {
    public void evaluatesExpression() {
        Ex1_Calculator calculator = new Ex1_Calculator();
        Double sum = calculator.evaluate("1+2+3");
        if (sum != 6) {
            System.err.println("Sum of evaluatesExpression was expeted to be 6 but was " + sum);
        } else {
            System.out.println("Sum of evaluatesExpression correct");
        }
        //assertEquals(6, sum);
    }

    public void evaluatesExpressionFail() {
        Ex1_Calculator calculator = new Ex1_Calculator();
        Double sum = calculator.evaluate("1+2+3+1");
        if (sum != 6) {
            System.err.println("Sum of evaluatesExpressionFail was expeted to be 6 but was " + sum);
        } else {
            System.out.println("Sum of evaluatesExpressionFail correct");
        }
        //assertEquals(6, sum);
    }

    public static void main(String[] args) {
        Ex1_CalculatorTest_naive test = new Ex1_CalculatorTest_naive();
        test.evaluatesExpression();
        test.evaluatesExpressionFail();
        // how do I count tests that are success and these that fail?
        // return boolean or 0 and 1?
    }
}
