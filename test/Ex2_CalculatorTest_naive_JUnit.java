import static org.junit.jupiter.api.Assertions.assertEquals;

import naive.Ex1_Calculator;
import org.junit.jupiter.api.Test;

public class Ex2_CalculatorTest_naive_JUnit {
    @Test
    public void evaluatesExpression() {
        Ex1_Calculator calculator = new Ex1_Calculator();
      Double sum = calculator.evaluate("1+2+3");
        assertEquals(6, sum);
    }

    @Test
    public void evaluatesExpressionFail() {
        Ex1_Calculator calculator = new Ex1_Calculator();
      Double sum = calculator.evaluate("1+2+3+1");
        assertEquals(6, sum);
    }
}
