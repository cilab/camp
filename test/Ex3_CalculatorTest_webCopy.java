import org.junit.jupiter.api.Test;
import webCopy.Ex2_Calculator;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Ex3_CalculatorTest_webCopy {
    @Test
    public void evaluatesExpression() {
        Ex2_Calculator calculator = new Ex2_Calculator();
      Double sum = calculator.evaluate("1+2+3");
        assertEquals(6, sum);
    }

    @Test
    public void evaluatesExpressionFail() {
        Ex2_Calculator calculator = new Ex2_Calculator();
      Double sum = calculator.evaluate("1+2+3+1");
        assertEquals(6, sum);
    }
}
